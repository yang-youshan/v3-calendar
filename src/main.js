import { createApp } from 'vue'
import App from './App.vue'
import BaiduCalendar from "vue-baidu-calendar"
const app=createApp(App)
app.use(BaiduCalendar)
app.mount('#app')

// createApp(App).mount('#app')
